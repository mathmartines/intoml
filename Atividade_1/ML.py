import pandas as pd
import numpy as np
from typing import Callable
import sklearn.cluster
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from Func import correct_color_label
from DataProcessing import data_pre_processing_general_case, data_preparation_new, data_preparation_exclude_categorical
from sklearn.cluster import AgglomerativeClustering, KMeans, DBSCAN, MiniBatchKMeans
from graphs import scatter_plot, horizontal_bar_plot, histogram_plot, SnsAPIConfiguration, line_plot
from matplotlib.colors import ListedColormap
from sklearn.metrics import silhouette_score, silhouette_samples


class ClusterAPI:

    def __init__(self,
                 data_frame: pd.DataFrame, data_processing: Callable, cluster_class: sklearn.cluster,
                 n_components: float, use_pca: bool = False
                 ):
        self.data_frame_ = data_frame
        self.data_processing_ = data_processing
        self.cluster_class_ = cluster_class
        self.pca_ = PCA(n_components=n_components)
        self.X_ = None
        self.x_processed = None
        self.use_pca_ = use_pca

    def fit(self, **kwargs):
        cluster = self.cluster_class_(**kwargs).fit(self.X)
        return cluster.labels_

    def prepare_X(self):
        if self.X_ is None:
            self.x_processed, _ = self.data_processing_(self.data_frame_)
            # change when needed
            self.X_ = self.pca_.fit_transform(self.x_processed) if self.use_pca_ else self.x_processed
        return self.X_

    @property
    def X(self):
        return self.X_ if self.X_ is not None else self.prepare_X()


if __name__ == '__main__':
    # reading data from file and correcting the Color feature
    stars_data = pd.read_csv("Stars.csv")
    stars_data['Color'] = stars_data['Color'].apply(correct_color_label, axis=1)

    # cluster
    cluster_api = ClusterAPI(
        data_frame=stars_data,
        data_processing=data_preparation_new,
        cluster_class=DBSCAN,
        n_components=0.91,
        use_pca=True
    )

    # For one plot
    # hierarquico
    # labels = cluster_api.fit(n_clusters=6, linkage='ward')

    # kmeans
    # labels = cluster_api.fit(n_clusters=6, n_init='auto')

    # dbscan
    labels = cluster_api.fit(eps=1.0, min_samples=11)

    # Plots
    sns_api = SnsAPIConfiguration()

    # instantiating the class responsible to configure the plots
    # Create a ListedColormap with the specified colors
    sns_api.plot(
        plot_strategy=scatter_plot,
        filename="Plots/ML/DBSCAN/DBSCAN2.pdf",
        x_axis=stars_data['Temperature'],
        y_axis=stars_data['A_M'],
        hue=labels,
        color_map=plt.get_cmap("jet"),
        x_label="Temperatura",
        y_label="A_M",
        title="Temperatura vs A_M"
    )
    print(f"Silhouette score: {silhouette_score(cluster_api.X, labels)}")

    # silhouette_score
    # nclusters = np.arange(2, 30, 1)
    # scores, points_bad = np.empty(len(nclusters)), np.empty(len(nclusters))
    # for index in range(len(nclusters)):
    #     labels = cluster_api.fit(n_clusters=nclusters[index], n_init='auto')
    #     scores[index] = silhouette_score(cluster_api.x_processed, labels)
    #     sample_sl = silhouette_samples(cluster_api.X, labels)
    #     mask = sample_sl < 0
    #     points_bad[index] = (100 * mask.sum() / len(mask))
    # #
    # sns_api.plot(
    #     plot_strategy=line_plot,
    #     filename="Plots/ML/KMeans/Silhouete_KG.pdf",
    #     x_axis=nclusters,
    #     y_axis=scores,
    #     x_label="número de clusters",
    #     y_label="Silhouette score",
    #     title=""
    # )
    # sns_api.plot(
    #     plot_strategy=line_plot,
    #     # filename="Plots/data_temp_am_type.pdf",
    #     x_axis=nclusters,
    #     y_axis=points_bad,
    #     x_label="número de clusters",
    #     y_label="Silhouette score",
    #     title=""
    # )
    # print(scores)
    # print(f"Best ratio: {nclusters[(scores / points_bad).argmax()]}")
    # print(f"Best Silhueta: {nclusters[scores.argmax()]}")