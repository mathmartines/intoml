"""Finds the Principal Components of the data set."""

import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from DataProcessing import data_pre_processing_general_case, data_preparation_new, data_preparation_exclude_categorical
from Func import correct_color_label
from graphs import scatter_plot, horizontal_bar_plot, histogram_plot, SnsAPIConfiguration


def find_index(vector: np.array, index_bin: int = 1) -> int:
    norm = np.sqrt(np.sum(np.square(vector[:index_bin])))
    if norm >= 0.9:
        return index_bin
    return find_index(vector, index_bin + 1)


if __name__ == "__main__":
    # reading data from file and correcting the Color feature
    stars_data = pd.read_csv("Stars.csv")
    stars_data['Color'] = stars_data['Color'].apply(correct_color_label, axis=1)

    # processing the data
    X, transformer = data_preparation_new(stars_data)
    # using the PCA
    pca = PCA()
    pca.fit(X)
    number_of_components = np.argmax(np.cumsum(pca.explained_variance_ratio_) >= 0.91) + 1

    print("Explained variance ratio:")
    print(", ".join([f"{ratio: .2f}" for ratio in pca.explained_variance_ratio_]))
    print(f"Number of components needed to account for 90% of the data variance: {number_of_components}")

    # name of the columns of the data
    features = np.concatenate((['Temperature', 'L', 'R', 'A_M'], transformer.categories_[0], transformer.categories_[1]))
    index = 2
    component_normalized = pca.components_[index] / np.linalg.norm(pca.components_[index])
    pca_components = pd.Series({feature: abs(component) for feature, component in zip(features, component_normalized)})

    # components that contain at least 80% of the norm
    abs_components = np.abs(component_normalized)
    abs_components.sort()
    print(abs_components[::-1])
    print(f"Index {find_index(abs_components[::-1])}")

    sns_api = SnsAPIConfiguration()
    sns_api.plot(
        plot_strategy=horizontal_bar_plot,
        filename="Plots/PCA/PCA_3_General.pdf",
        pd_series=pca_components,
        color='#a3b18a',
        y_label="Características",
        x_label="Peso",
        title="Terceira Componente Normalizada"
    )