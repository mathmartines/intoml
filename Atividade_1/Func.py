
def correct_color_label(color_label: str, *args, **kwargs) -> str:
    """
    Defines a pattern format for the Color feature from the Stars.csv dataset.

    The format is defined as follows:
        1. all words must be lowercase;
        2. if the label has more than one letter, the colors must be separated by a -;
        3. the words are sorted by alphabetical order.

    Example: Yellowish White goes to white-yellowish.

    :param color_label: string with the label we need to format.
    :return: the same color label with the specific format we choose.
    """
    lower_case = color_label.lower()
    return "-".join(sorted(lower_case.split("-") if "-" in lower_case else lower_case.split()))
