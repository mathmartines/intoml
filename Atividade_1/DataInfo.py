""""
Reads information from the data file and generates some plots.
"""

import pandas as pd
import numpy as np
from Func import correct_color_label
from graphs import scatter_plot, horizontal_bar_plot, histogram_plot, SnsAPIConfiguration
from matplotlib.colors import ListedColormap


if __name__ == "__main__":
    # reading data from file and correcting the Color feature
    stars_data = pd.read_csv("Stars.csv")
    stars_data['Color'] = stars_data['Color'].apply(correct_color_label, axis=1)

    # getting the general information about the dataset
    print(stars_data.info())
    print(stars_data.describe())

    # instantiating the class responsible to set up the plots
    sns_api = SnsAPIConfiguration()

    # Plotting
    generate_figures = False
    if generate_figures:
        # -----------------------------------------------------------------------------------
        # SCATTER PLOT
        # Create a ListedColormap with the specified colors
        colors = ['#780000', '#c1121f', '#606c38', '#003049', "#669bbc", "#bc6c25", "black"]
        custom_cmap = ListedColormap(colors)
        sns_api.plot(
            plot_strategy=scatter_plot,
            # filename="Plots/data_temp_am_type.pdf",
            x_axis=stars_data['Temperature'],
            y_axis=stars_data['A_M'],
            hue=stars_data['Type'],
            color_map=custom_cmap,
            x_label="Temperatura",
            y_label="A_M",
            title=""
        )

        # -----------------------------------------------------------------------------------
        # HISTOGRAM PLOT
        sns_api.plot(
            plot_strategy=histogram_plot,
            filename="Plots/InfoData/AM.pdf",
            x_axis=stars_data['A_M'],
            bins=20,
            x_label="A_M",
            y_label="Número de estrelas",
            title=""
        )

        # -----------------------------------------------------------------------------------
        # HORIZONTAL BAR PLOT
        # averaged temperature for the stars
        feature = 'Colors'
        colors = np.unique(stars_data[feature])
        avg_temp = pd.Series({color: stars_data[stars_data[feature] == color]['Temperature'].mean()
                              for color in colors})
        sns_api.plot(
            plot_strategy=horizontal_bar_plot,
            filename="Plots/sp_L.pdf",
            pd_series=avg_temp,
            color='#a3b18a',
            x_label="A_M",
            y_label="Spectral Class",
            title=""
        )
