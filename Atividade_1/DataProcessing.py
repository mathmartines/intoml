"""Routines for preparing the data for the analysis"""
from typing import Tuple, Any
import numpy as np
import pandas as pd
from collections import OrderedDict
from Func import correct_color_label
from sklearn.preprocessing import OneHotEncoder, StandardScaler, MinMaxScaler, QuantileTransformer
from sklearn.compose import ColumnTransformer
from sklearn.base import BaseEstimator, TransformerMixin


class ColorTransformer(BaseEstimator, TransformerMixin):
    """
    Transforms the Color feature to a numerical values.
    First the colors are sorted in increasing mean value of the temperature for all
    stars of a given color.

    Then the Color which is related to the smallest temperature is assigned to the 0
    and the largest temperature is assigned to the number of categories - 1.
    """

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        # finding the average of the temperature for each unique color
        unique_colors = np.unique(X[:, 4])
        avg_temp = {
            current_color: np.mean([temp for temp, _, _, _, color, *_ in X if color == current_color])
            for current_color in unique_colors
        }
        # sorting the colors according to the mean temperature
        sorted_colors = OrderedDict(sorted(avg_temp.items(), key=lambda item: item[1]))
        # defining the dictionary with the number
        colors_number = {color: number for color, number in zip(sorted_colors.keys(), range(len(unique_colors)))}
        X[:, 4] = np.array([colors_number[color] for color in X[:, 4]])
        # return the modified data
        return X


def data_pre_processing_general_case(stars_dataframe: pd.DataFrame) -> Tuple[np.ndarray, Any]:
    """
    Prepares the data for the ML algorithms.

    The data is prepared using the following strategy:
        1. the features that have continuum values are rescaled using the StandardScaler;
        2. the categorical features are transformed using the OneHotEncoder.

    :return: The data for the ML algorithm
    """
    # creates the column transformer
    columns_transformer = ColumnTransformer([
        ('standard_scaler', StandardScaler(), ['Temperature', 'L', 'R', "A_M"]),
        ('encoder', OneHotEncoder(), ['Color', 'Spectral_Class', 'Type'])
    ], remainder='passthrough')

    stars_data_transformed = columns_transformer.fit_transform(
        stars_dataframe[['Temperature', 'L', 'R', 'A_M', 'Color', 'Spectral_Class']]
    )

    return stars_data_transformed.toarray(), columns_transformer


def data_preparation_exclude_categorical(stars_dataframe: pd.DataFrame) -> Tuple[np.ndarray, Any]:
    """Prepares the data excluding the categorical features."""
    stars_dataframe = stars_dataframe.drop(columns=['Type', "Color", 'Spectral_Class'])
    standard_scaler = StandardScaler()
    stars_data_transformed = standard_scaler.fit_transform(stars_dataframe[['Temperature', 'L', 'R', 'A_M']])
    return stars_data_transformed, standard_scaler


def data_preparation_new(stars_dataframe: pd.DataFrame) -> Tuple[np.ndarray, Any]:
    """
    Excludes Type feature. And allows the creation of three categories.

    :param stars_dataframe:
    :return:
    """
    # dropping type feature
    # stars_dataframe = stars_dataframe.drop(columns=['Type'])
    # categorical feature
    one_hot_encoder = OneHotEncoder()
    categorical_features = one_hot_encoder.fit_transform(stars_dataframe[['Color', 'Spectral_Class', 'Type']])
    # numerical features
    numerical_features = np.log10(stars_dataframe[['Temperature', 'L', 'R']].to_numpy())
    numerical_features = np.concatenate((numerical_features, stars_dataframe[['A_M']].to_numpy()), axis=1)
    # scaling using the min max scaler
    numerical_features = StandardScaler().fit_transform(numerical_features)

    return (np.array(np.concatenate((numerical_features, categorical_features.todense()), axis=1))
            , one_hot_encoder)


def data_pre_processing(stars_dataframe: pd.DataFrame) -> Tuple[np.ndarray, Any]:
    """
    Prepares the Stars data in the following way.

    1. It uses the OneHotEncoder to transform the Type and Spectral_Class categorical features.
    2. Transform the Color feature using ColorTransformer;
    3. It uses StandardScaler to transform the Temperature, L, R, A_M, and Color features.

    :return: the data for the ML algorithm.
    """
    X = ColorTransformer().transform(stars_dataframe.to_numpy())
    new_stars_dataframe = pd.DataFrame(X, columns=stars_dataframe.columns)

    columns_transformer = ColumnTransformer([
        ('standard_scaler', StandardScaler(), ['Temperature', 'L', 'R', 'A_M', 'Color']),
        ('encoder', OneHotEncoder(), ['Spectral_Class', 'Type'])
    ], remainder='passthrough')

    stars_data_transformed = columns_transformer.fit_transform(
        new_stars_dataframe[['Temperature', 'L', 'R', 'A_M', 'Color', 'Spectral_Class', 'Type']]
    )

    return stars_data_transformed, columns_transformer


if __name__ == "__main__":
    # reading data from file and correcting the Color feature
    stars_data = pd.read_csv("Stars.csv")
    stars_data['Color'] = stars_data['Color'].apply(correct_color_label, axis=1)
    # processing the data for the ML algorithm
    # 1. using the general approach
    X1, _ = data_pre_processing_general_case(stars_data)
    # 2. using the ColorTransformer
    X2, _ = data_pre_processing(stars_data)
    # 3. excluding the categorical features
    X3, _ = data_preparation_exclude_categorical(stars_data)
    # 4. scaling T, L, and R with Log
    X4, one_hot = data_preparation_new(stars_data)
