import matplotlib.pyplot as plt
import seaborn as sns

sns.set(rc={'figure.figsize':(8, 4), 'text.usetex': True, "font.family": "sans-serif",
            "font.sans-serif": ["Time New Roman", "Times"]})


def scatter_plot(x_axis, y_axis, hue, color_map):
    """
    Creates a scatter plot using the Seaborn library.

    :param x_axis: The x-axis data
    :param y_axis: The y-axis data
    :param hue: The hue of the data
    :param color_map: The color to be used for the scatter plot
    """
    return sns.scatterplot(x=x_axis, y=y_axis, hue=hue, palette=color_map)


def histogram_plot(x_axis, bins):
    """
    Creates a histogram plot using the seaborn library.

    :param x_axis: data to create the histogram for
    :param bins: number of bins for the histogram
    """
    return sns.histplot(x=x_axis, bins=bins)


def horizontal_bar_plot(pd_series, color, rotation=0):
    """"
    Creates a horizontal bar plot using the seaborn library.

    :param pd_series: panda series
    :param color: the color for the bar plot
    """
    # sorting the values
    items_sorted = pd_series.sort_values(ascending=False)
    plt.yticks(rotation=rotation)
    return sns.barplot(x=items_sorted.to_numpy(), y=items_sorted.index, color=color, orient="h")


def line_plot(x_axis, y_axis):
    return sns.lineplot(x=x_axis, y=y_axis, marker='o')


class SnsAPIConfiguration:
    """Uses the API provided by Seaborn to generate useful plots."""

    def __init__(self, default_theme: str = 'notebook', font_scale: float = 2):
        self.default_theme_ = default_theme
        self.font_scale_ = font_scale

    def config_figures_(self):
        sns.set_theme(self.default_theme_, font_scale=self.font_scale_)
        sns.set_style('ticks')

    def plot(self, plot_strategy, x_label: str, y_label: str, title: str,  filename=None, **kwargs):
        self.config_figures_()
        sns_plt = plot_strategy(**kwargs)
        sns_plt.set_xlabel(x_label)
        sns_plt.set_ylabel(y_label)
        sns_plt.set_title(title)
        if filename is not None:
            plt.savefig(filename, format="pdf", bbox_inches="tight", dpi=300)
        plt.show()

