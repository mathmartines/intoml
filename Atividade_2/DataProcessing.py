import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.impute import KNNImputer


def DataProcessing():
    data = pd.read_csv('dados_filtrados.csv', index_col=0)

    # 1. Mantendo somente as colunas TP_ESCOLA e as notas de cada aluno
    data = data[["TP_ESCOLA", "NU_NOTA_CN", "NU_NOTA_CH", "NU_NOTA_LC", "NU_NOTA_REDACAO", "NU_NOTA_MT"]]

    # 2. Alterando o significado da feature TP_ESCOLA:
    #    Valor 1: veio de escola particular
    #    Valor 0: não declarou que veio de escola particular ou veio de escola pública
    data.loc[data['TP_ESCOLA'] != 3, 'TP_ESCOLA'] = 0
    data.loc[data['TP_ESCOLA'] == 3, 'TP_ESCOLA'] = 1

    # 2. Notas zero e notas vazias serão tratadas da mesma maneira
    #    Uma nota zero significa que algo aconteceu (eliminado, por exemplo).
    #    Vamos assumir que o nosso modelo representa o caso ideal, onde esse tipo de anomalia
    #    não aconteceu
    for nota in ['NU_NOTA_CN', 'NU_NOTA_CH', 'NU_NOTA_LC', 'NU_NOTA_REDACAO', 'NU_NOTA_MT']:
        data.loc[data[nota] == 0., nota] = np.nan

    # 3. Excluindo dados (linhas) onde não há nenhuma nota (a pessoa faltou no ENEM 2016)
    indices_faltantes = data[
        data[['NU_NOTA_CN', 'NU_NOTA_CH', 'NU_NOTA_LC', 'NU_NOTA_REDACAO', 'NU_NOTA_MT']].isna().all(axis=1)].index
    data = data.drop(labels=indices_faltantes, axis=0)

    # 4. Como o nosso objetivo é prever as notas de matemática, para os alunos que faltaram no dia da prova de Redação,
    #    Linguagens e Matemática, se torna dificil determinar as suas notas nesse dia com base apenas nas notas
    #    das provas de Ciências Humanas e Ciências da Natureza. Por conta disso, vamos eliminar as linhas que
    #    correspondem a alunos que faltaram no segundo dia.
    index_faltaram_sg_dia = data[data[['NU_NOTA_REDACAO', 'NU_NOTA_LC', 'NU_NOTA_MT']].isna().all(axis=1)].index
    data = data.drop(labels=index_faltaram_sg_dia, axis=0)

    index_faltaram_pr_dia = data[data[['NU_NOTA_CN', 'NU_NOTA_CH']].isna().all(axis=1)].index
    data = data.drop(labels=index_faltaram_pr_dia, axis=0)

    # 5. Usando o MinMaxScaler para deixar as notas na ordem de grandeza igual a 1. Caso isso não seja feito,
    #    a informação sobre o TP_ESCOLA será totalmente inútil.
    scaler = MinMaxScaler()
    dados_escalonados = scaler.fit_transform(
        data[['NU_NOTA_CN', 'NU_NOTA_CH', 'NU_NOTA_LC', 'NU_NOTA_REDACAO', 'NU_NOTA_MT']])
    # juntando dados
    tp_escola = data[['TP_ESCOLA']].to_numpy()
    dados_escalonados = np.hstack((tp_escola, dados_escalonados))

    # 6. Lindando com valores faltantes: vamos usar o K-neighboor para preencher os dados que faltam.
    #    Utilizaremos o valor k = 10.
    imputer = KNNImputer(n_neighbors=10, weights="uniform")
    dados_finais = imputer.fit_transform(dados_escalonados)

    return dados_finais


if __name__ == '__main__':
    dados_preparados = DataProcessing()
    df_dados = pd.DataFrame(dados_preparados, columns=["TP_ESCOLA", "NU_NOTA_CN", "NU_NOTA_CH",
                                                       "NU_NOTA_LC", "NU_NOTA_REDACAO", "NU_NOTA_MT"])
    print(df_dados.info())
    print(df_dados.corr())
