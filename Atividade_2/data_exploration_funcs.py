"""Funções utilizadas para exploração de dados"""
import pandas as pd
import numpy as np
from typing import Dict, List


def notas_por_categoria(
        data_frame: pd.DataFrame, restricoes: Dict[str, List[int]], coluna_alvo: str, drop_nan: bool = True
) -> pd.DataFrame:
    """
    Retorna as notas de matemática considerando as restrições no dicionário restricoes.

    :param coluna_alvo: Coluna que queremos extrair as informações
    :param data_frame: tabela do conjunto de dados
    :param restricoes: dicionário de restrições.
        A chave representa o nome da coluna, enquanto o valor da chave correspondete representa
        os possíveis valores que aquela coluna pode assumir.

    :return: o mesmo data_frame, mas com as restricoes pedidas e sem incluir dados vazios
    """
    # criando a máscara
    mask_ = [
        all(row[coluna] in valores_permitidos for coluna, valores_permitidos in restricoes.items())
        for _, row in data_frame.iterrows()
    ]
    # retornando somente os valores não vazios
    return data_frame[mask_][coluna_alvo].dropna() if drop_nan else data_frame[mask_][coluna_alvo]


def estatistica_coluna_categorica(data_frame: pd.DataFrame, coluna: str, coluna_alvo: str) -> Dict[int, Dict[str, float]]:
    """
    Printa o valor da média, desvio padrão e o número de ocorrências da coluna alvo para uma determinada
    categoria da coluna.

    :param coluna_alvo: Coluna que queremos a informação estatística
    :param data_frame: Conjunto de dados
    :param coluna: nome da coluna que representa uma variável categórica
    """
    # listando todas as possíveis categorias
    possiveis_categorias = np.unique(data_frame[coluna].dropna())
    # dicionário onde a chave representa a categoria
    categoria_info = {
        categoria: {"mean": 0, "std": 0, "count": 0} for categoria in possiveis_categorias
    }

    for categoria in possiveis_categorias:
        print("-------------------------------------")
        print(f"Info para categoria {categoria}")
        notas = notas_por_categoria(data_frame, {coluna: [categoria]}, coluna_alvo)
        categoria_info[categoria] = {info: getattr(notas, info)() for info in ["mean", "std", "count"]}
        print(f"Média: {categoria_info[categoria]['mean']}")
        print(f"Std: {categoria_info[categoria]['std']}")
        print(f"Numero de ocorrencias: {categoria_info[categoria]['count']}")

    return categoria_info
